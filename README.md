# Bluetooth Finder

Simple app that displays a list of Bluetooth enabled devices near your location

# Preview
![Alt text](https://i.imgur.com/zRAuE9w.png)


# Features
* Shows the name and MAC address of devices near you
* Shows signal strength in dBm so you know how close device is


# Built With
* Java 8
* Butterknife
* Android Studio
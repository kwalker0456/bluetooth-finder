package com.example.bluetoothfinder.Utils;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.example.bluetoothfinder.R;

public class BluetoothHelper {

    private static final String TAG = BluetoothHelper.class.getName();

    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 2222;

    private boolean mHasLocationPermission = false;

    private Context mContext;

    public BluetoothHelper(Context mContext) {
        this.mContext = mContext;
    }

    public boolean mhasLocationPermission() {
        if (ContextCompat.checkSelfPermission(mContext, FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            new AlertDialog.Builder(getmContext())
                    .setTitle(getmContext().getResources()
                            .getString(R.string.dialog_request_location_permission_title))
                    .setMessage(getmContext().getResources()
                            .getString(R.string.dialog_request_location_permission_message))
                    .setPositiveButton(getmContext().getResources()
                            .getString(R.string.dialog_request_location_permission_postive),
                            new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ActivityCompat.requestPermissions((Activity) getmContext(),
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    LOCATION_PERMISSION_REQUEST_CODE);
                            // They said yes, so set to true
                            mHasLocationPermission = true;
                        }
                    })
                    .setNegativeButton(getmContext().getResources()
                            .getString(R.string.dialog_request_location_permission_negative),
                            new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Toast.makeText(mContext, R.string.location_not_available, Toast.LENGTH_SHORT).show();
                            // Said no, set to false
                            mHasLocationPermission = false;
                        }
                    }).show();
        } else {
            mHasLocationPermission = true;
        }

        return mHasLocationPermission;
    }

    public Context getmContext() {
        return mContext;
    }

}

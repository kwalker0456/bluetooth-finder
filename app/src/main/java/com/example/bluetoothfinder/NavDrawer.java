package com.example.bluetoothfinder;

import android.bluetooth.BluetoothDevice;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.example.bluetoothfinder.Bluetooth.BluetoothFragment;
import com.example.bluetoothfinder.Bluetooth.CustomBluetoothDevice;

public class NavDrawer extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        BluetoothFragment.BluetoothCallback {

    private static final String TAG = NavDrawer.class.getName();

    private FragmentManager fragmentManager = getSupportFragmentManager();
    private FragmentTransaction fragmentTransaction;
    private BluetoothFragment bluetoothFragment;

    private String currentFragmentTAG = "";
    private static final String bluetoothFragmentTag = "bluetoothFragmentTag";

    private FrameLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Set nav icon color/tint to allow colored icons to appear
        navigationView.setItemTextColor(null);
        navigationView.setItemIconTintList(null);

        // Set up Main Fragment
        initHomeFragmentTransaction();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_bluetooth) {
            if (bluetoothFragment == null) {
                bluetoothFragment = new BluetoothFragment();
            }
            setCurrentFragment(bluetoothFragment);

            setTitle(getResources().getString(R.string.nav_bluetooth_fragment));

        } else if (id == R.id.nav_gallery) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    // This method sets the default fragment to display
    private void initHomeFragmentTransaction() {

        container = findViewById(R.id.container);
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.commit();
        bluetoothFragment = new BluetoothFragment();
        setCurrentFragment(bluetoothFragment);
    }

    // Custom method to show/hide currentFragment
    public void setCurrentFragment(Fragment fragment) {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_from_left, R.anim.slide_out_to_right);
        // If fragment attempting to show already been instantiated AND other fragments that have been displayed
        if (fragmentManager.getFragments().contains(fragment)) {
            // Quit process if user clicks on fragment that has already been displayed
            if (currentFragmentTAG.equals(getTagForFragment(fragment))) {
                return;
            }
            // If not empty, a fragment exists
            if (!fragmentManager.getFragments().isEmpty()) {
                ft.hide(getFragmentForTag());
            }
            setCurrentFragmentTAG(fragment);
            ft.show(fragment).commit();
        } else {
            if (!fragmentManager.getFragments().isEmpty()) {
                ft.hide(getFragmentForTag());
            }
            setCurrentFragmentTAG(fragment);
            ft.add(R.id.container, fragment, currentFragmentTAG).commit();
        }
    }

    private Fragment getFragmentForTag() {
        Fragment fragment;
        switch (currentFragmentTAG) {
            case bluetoothFragmentTag:
                fragment = bluetoothFragment;
                break;
            default:
                fragment = bluetoothFragment;
                break;
        }
        return fragment;
    }

    // Gets the Tag for a given Fragment
    private String getTagForFragment(Fragment fragment) {
        String fragmentTag = "";
        if (fragment instanceof BluetoothFragment) {
            fragmentTag = bluetoothFragmentTag;

            return fragmentTag;
        }
        return fragmentTag;
    }

    // Sets currentFragmentTAG to be equal the fragment that is currently visible
    private void setCurrentFragmentTAG(Fragment fragment) {
        if (fragment instanceof BluetoothFragment) {
            currentFragmentTAG = bluetoothFragmentTag;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onBluetoothFragmentInteraction(CustomBluetoothDevice customBluetoothDevice) {

    }
}

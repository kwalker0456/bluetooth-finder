package com.example.bluetoothfinder.Bluetooth;

public class CustomBluetoothDevice {

    private String deviceName = "";
    private String deviceID = "";
    private String deviceRSSI = "";

    public CustomBluetoothDevice() {}

    public String getDeviceName() { return deviceName; }

    public void setDeviceName(String deviceName) { this.deviceName = deviceName; }

    public String getDeviceID() { return deviceID; }

    public void setDeviceID(String deviceID) { this.deviceID = deviceID; }

    public String getDeviceRSSI() { return deviceRSSI; }

    public void setDeviceRSSI(String deviceRSSI) { this.deviceRSSI = deviceRSSI; }

}

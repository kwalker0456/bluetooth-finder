package com.example.bluetoothfinder.Bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.bluetoothfinder.R;
import com.example.bluetoothfinder.Utils.BluetoothHelper;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

public class BluetoothFragment extends Fragment
        implements View.OnClickListener {

    private static final String TAG = BluetoothFragment.class.getName();

    private int mColumnCount = 1;

    private BluetoothFragment.BluetoothCallback mListener;
    private BluetoothAdapter mBluetoothAdapter;

    public ArrayList<CustomBluetoothDevice> mBluetoothDevices = new ArrayList<>();
    public BluetoothListAdapter mBluetoothListAdapter;
    private BluetoothHelper mBluetoothHelper;

    @BindView(R.id.btn_discover_bluetooth_devices) Button btn_discover_bluetooth_devices;
    @BindView(R.id.bluetooth_recycler_view) RecyclerView bluetoothRecyclerView;

    private View mView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BluetoothFragment.BluetoothCallback) {
            mListener = (BluetoothFragment.BluetoothCallback) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement BlueToothFragmentCallback");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get default device Bluetooth Adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // Set up helper class
        mBluetoothHelper = new BluetoothHelper(this.getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.bluetooth_fragment_list, container, false);

        // Bind the views
        ButterKnife.bind(this, mView);

        // Set up listener
        btn_discover_bluetooth_devices.setOnClickListener(this);

        // Set the adapter
        if (mView instanceof RecyclerView) {
            Context context = mView.getContext();
            bluetoothRecyclerView = (RecyclerView) mView;
            if (mColumnCount <= 1) {
                bluetoothRecyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                bluetoothRecyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
        }

        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        getActivity().setTitle(getResources().getString(R.string.nav_bluetooth_fragment));
    }

    @Override
    public void onDetach() {
        super.onDetach();

        // Remove listener
        mListener = null;
    }


    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.btn_discover_bluetooth_devices:

                // Ensure that list is clear before button is pressed to prevent duplicates
                mBluetoothDevices.clear();

                // Begin Bluetooth discovery
                startBluetoothDiscovery();
        }
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            Log.d(TAG, "from onReceive - Device Found");

            if (action.equals(BluetoothDevice.ACTION_FOUND)){

                // Obtain Bluetooth Device data from Intent
                BluetoothDevice bluetoothDevice = intent.getParcelableExtra (BluetoothDevice.EXTRA_DEVICE);

                // Obtain RSSI value
                int rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, Short.MIN_VALUE);

                // Set values on custom object
                CustomBluetoothDevice customBluetoothDevice = new CustomBluetoothDevice();
                customBluetoothDevice.setDeviceName(bluetoothDevice.getName());
                customBluetoothDevice.setDeviceID(bluetoothDevice.getAddress());
                customBluetoothDevice.setDeviceRSSI(String.valueOf(rssi));

                // Append to list
                mBluetoothDevices.add(customBluetoothDevice);

                // Refresh the Recycler View
                resetRecyclerView();

                Log.d(TAG, "from onReceive: " + bluetoothDevice.getName() + ": " + bluetoothDevice.getAddress());
            }
        }
    };

    public void startBluetoothDiscovery() {
        Log.d(TAG, "startBluetoothDiscovery: Looking for unpaired devices.");

        // If it is already in the process of discovering, do not start a second time
        if(mBluetoothAdapter.isDiscovering()){
            mBluetoothAdapter.cancelDiscovery();
            Log.d(TAG, "startBluetoothDiscovery: Canceling discovery.");

            // Verify that we have permission before starting discovery
            if (mBluetoothHelper.mhasLocationPermission()) {
                mBluetoothAdapter.startDiscovery();
                IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                getActivity().registerReceiver(mBroadcastReceiver, discoverDevicesIntent);
            } else {
                Toast.makeText(getActivity(), R.string.location_permission_declined, Toast.LENGTH_SHORT).show();
            }
        }

        if(!mBluetoothAdapter.isDiscovering()){
            if (mBluetoothHelper.mhasLocationPermission()) {
                mBluetoothAdapter.startDiscovery();
                IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                getActivity().registerReceiver(mBroadcastReceiver, discoverDevicesIntent);
            }
        }
    }

    public void resetRecyclerView() {
        mBluetoothListAdapter = new BluetoothListAdapter(mBluetoothDevices, mListener);
        bluetoothRecyclerView.setAdapter(mBluetoothListAdapter);
    }

    public interface BluetoothCallback {
        void onFragmentInteraction(Uri uri);
        void onBluetoothFragmentInteraction(CustomBluetoothDevice bluetoothDevice);
    }
}

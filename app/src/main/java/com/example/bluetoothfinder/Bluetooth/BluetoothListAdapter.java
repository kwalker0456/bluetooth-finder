package com.example.bluetoothfinder.Bluetooth;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.bluetoothfinder.R;

import java.util.ArrayList;

public class BluetoothListAdapter extends RecyclerView.Adapter<BluetoothListAdapter.ViewHolder> {

    private final ArrayList<CustomBluetoothDevice> mValues;
    private final BluetoothFragment.BluetoothCallback mListener;

    public BluetoothListAdapter(ArrayList<CustomBluetoothDevice> items, BluetoothFragment.BluetoothCallback listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_bluetooth_list_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.txt_device_name.setText(mValues.get(position).getDeviceName());
        holder.txt_device_id.setText(String.valueOf(mValues.get(position).getDeviceID()));
        holder.txt_device_rssi.setText(String.valueOf(mValues.get(position).getDeviceRSSI()));
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onBluetoothFragmentInteraction(holder.mItem);

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView txt_device_name;
        public final TextView txt_device_id;
        public final TextView txt_device_rssi;
        public CustomBluetoothDevice mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            txt_device_name = view.findViewById(R.id.txt_device_name);
            txt_device_id = view.findViewById(R.id.txt_device_id);
            txt_device_rssi = view.findViewById(R.id.txt_device_rssi);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }
}
